FROM python:3.6.9
WORKDIR /social_aiohttp
ENV PYTHONBUFFERED True

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY social_aiohttp/ .
