from pydantic import BaseModel


async def make_unique_id(collection):
    last_item_cursor = collection.find(projection={'_id': False}).sort('id', -1)
    last_item = await last_item_cursor.to_list(length=1)

    item_id = 0
    if last_item:
        item_id = last_item[0]['id']

    return {'id': int(item_id) + 1}


class Post(BaseModel):

    id: int
    title: str
    text: str

    @staticmethod
    async def get_by_id(db, post_id):
        try:
            post = await db.posts.find_one({'id': int(post_id)})
        except:
            return {}
        return post

    @staticmethod
    async def create(db, data):
        if isinstance(data, dict):
            data.update(await make_unique_id(db.posts))
        post = Post(**data)
        data.update({'like': '>_<'})
        await db.posts.insert_one(data)
        return post.id

    @staticmethod
    async def update(db, data):
        if 'id' not in data:
            return {}
        await db.posts.update_one({'id': data.pop('id')}, {'$set': data})

    @staticmethod
    async def delete(db, post_id):
        await db.posts.delete_one({'id': post_id})


class Comment(BaseModel):
    text: str
    id: int
    post_id: int

    @staticmethod
    async def get_by_id(db, comment_id):
        try:
            comment = await db.comments.find_one({'id': int(comment_id)})
        except:
            return {}
        return comment

    @staticmethod
    async def get_all_by_post_id(db, post_id):
        cursor = db.comments.find({'post_id': post_id})
        return await cursor.to_list(length=100)

    @staticmethod
    async def create(db, data):
        if isinstance(data, dict):
            data.update(await make_unique_id(db.comments))

        comment = Comment(**data)
        await db.comments.insert_one(data)
        return comment.id

    @staticmethod
    async def delete(db, comment_id):
        await db.comments.delete_one({'id': comment_id})
