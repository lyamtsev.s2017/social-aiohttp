import aiohttp_jinja2
import jinja2
from aiohttp import web

from routes import setup_routes
from settings import Settings
import mongo
import aioreloader


def make_app():
    app = web.Application()
    app['settings'] = Settings()
    aiohttp_jinja2.setup(app,
                         loader=jinja2.FileSystemLoader('templates'))
    app['static_root_url'] = '/static'
    setup_routes(app)
    mongo.setup(app)
    return app


def main():
    aioreloader.start()
    app = make_app()
    web.run_app(app)


if __name__ == '__main__':
    main()
