from aiohttp import web

import views


def reverse(request, url_name):
    return request.app.router[url_name].url_for()


def setup_routes(app):
    app.add_routes([
        web.view('/', views.IndexView, name='index'),
        web.view('/create', views.CreatePostView, name='create'),
        web.view('/update', views.UpdatePostView, name='update'),
        web.view('/posts', views.PostListView, name='posts'),
        web.view('/delete', views.PostDeleteView, name='delete'),
        web.view('/post_detail', views.PostDetailView, name='detail'),
        web.view('/ajax_like', views.AjaxLikeDislikeView, name='like'),

        web.view('/add_comment', views.CreateCommentView, name='create-comment'),
        web.view('/delete_comment', views.DeleteCommentView, name='delete-comment')
    ])
