import aiohttp_jinja2
from aiohttp import web

from models import Post, Comment
import routes


class IndexView(web.View):

    @aiohttp_jinja2.template('index.html')
    async def get(self):
        last_post = self.request.app['mongo'].posts.find().sort('id', -1)
        last_post_data = await last_post.to_list(length=1)
        last_post_data.append({})
        return {'post': last_post_data}


class CreatePostView(web.View):

    @aiohttp_jinja2.template('create_post.html')
    async def get(self):
        return {'post': ''}

    async def post(self):
        data = await self.request.post()
        post_data = {'title': data.get('title') or 'No title',
                     'text': data.get('text') or 'No text'}
        await Post.create(self.request.app['mongo'], post_data)

        return web.HTTPFound(routes.reverse(self.request, 'index'), )


class UpdatePostView(web.View):

    @aiohttp_jinja2.template('update_post.html')
    async def get(self):
        post = await Post.get_by_id(self.request.app['mongo'], self.request.query.get('id'))
        return {'post': post}

    @aiohttp_jinja2.template('update_post.html')
    async def post(self):
        data = await self.request.post()
        post_data = {'title': data.get('title') or 'No title',
                     'text': data.get('text') or 'No text',
                     'id': int(data.get('id'))}
        await Post.update(self.request.app['mongo'], post_data)

        return web.HTTPFound(routes.reverse(self.request, 'detail') + f'?id={data.get("id")}')


class PostListView(web.View):

    @aiohttp_jinja2.template('post_list.html')
    async def get(self):
        posts_cursor = self.request.app['mongo'].posts.find()
        posts = await posts_cursor.to_list(length=100)
        return {'posts': posts}


class PostDeleteView(web.View):

    async def post(self):
        data = await self.request.post()
        post_id = data.get('id')
        await Post.delete(self.request.app['mongo'], int(post_id))

        return web.HTTPFound(routes.reverse(self.request, 'posts'))


class PostDetailView(web.View):

    @aiohttp_jinja2.template('post_detail.html')
    async def get(self):
        post = await Post.get_by_id(self.request.app['mongo'], self.request.query.get('id'))
        comments = await Comment.get_all_by_post_id(self.request.app['mongo'], post.get('id'))
        return {'post': post,
                'comments': comments}


class AjaxLikeDislikeView(web.View):

    async def post(self):
        data = await self.request.post()
        post = await Post.get_by_id(self.request.app['mongo'], data.get('post_id'))
        action = post.get('like')
        if action == '<3':
            action_type = '>_<'
        else:
            action_type = '<3'
        await Post.update(self.request.app['mongo'], {'like': action_type,
                                                      'id': int(data.get('post_id'))})

        return web.json_response({'action_type': action_type})


class CreateCommentView(web.View):
    async def post(self):
        data = await self.request.post()
        cleaned_data = {
            'text': data.get('text') or 'No comment text',
            'post_id': int(data.get('post_id')),
        }
        await Comment.create(self.request.app['mongo'], cleaned_data)

        return web.HTTPFound(f'/post_detail?id={data.get("post_id")}')


class DeleteCommentView(web.View):
    async def post(self):
        data = await self.request.post()
        await Comment.delete(self.request.app['mongo'], int(data.get('id')))

        return web.HTTPFound(f'/post_detail?id={data.get("post_id")}')
