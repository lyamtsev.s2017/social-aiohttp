from pydantic import BaseSettings


class Settings(BaseSettings):
    db_name = 'social_db'
    mongo_uri = 'mongodb://social:social@social_db'
